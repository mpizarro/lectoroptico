import cx_Freeze

executables = [cx_Freeze.Executable("Lector.py",
                                 base = "Win32GUI",
                                 icon = None)]

build_exe_options = {"packages": ["cv2", "sys", "glob", "eyex_corev1", "numpy", "string", "_mysql", "xlsxwriter", "Queue"],
                     "include_files":["gui",
                                      "eyex_corev1/eye.py"]}

cx_Freeze.setup(
    name = "Lector Optico FalconEyes",
    version = "1.2.2",
    description = "L.O. AEDUC",
    options={"build_exe": build_exe_options},
    executables = executables
    )
