# -*- coding: utf-8 -*-

from PyQt4 import QtCore as QC, QtGui as QG, uic
from eyex_corev1 import eye
import sys
import glob
import Queue
import string
import os
import _mysql
import xlsxwriter

MainWin_UI = uic.loadUiType("gui/ventana.ui")[0]
Box_ImagenView_UI = uic.loadUiType("gui/visor_img.ui")[0]
RegisterKey_UI = uic.loadUiType("gui/registerkey.ui")[0]


class Ventana(QG.QMainWindow, MainWin_UI):
    def __init__(self, parent=None):
        QG.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.init()
        self.showMaximized()

    def init(self):
        self.btn_archivo.clicked.connect(self.selectFolder)
        self.btn_p_selec.clicked.connect(self.setConfProc)
        self.btn_p_cambiar.clicked.connect(self.changeConfProc)
        self.btn_proces.clicked.connect(self.preProcess)
        self.btn_viewimg.clicked.connect(self.showImage)
        self.export_test.clicked.connect(self.export_data)
        self.actionSalir.triggered.connect(lambda: self.close())

    def selectFolder(self):
        direct = QG.QFileDialog.getExistingDirectory(caption="Selecciona la Carpeta conetenedora",
                                                     options=QG.QFileDialog.DontResolveSymlinks)
        self.path_master = str(direct)
        self.txt_archivo.setText(direct)
        files = glob.glob(self.path_master + "/*"+str(self.ext_file.currentText()))
        if len(files) > 0:
            self.ArrayFiles = files
            self.addListFiles(files)
            self.btn_proces.setEnabled(True)
        else:
            print "err"
            msg = QG.QMessageBox()
            msg.setIcon(QG.QMessageBox.Critical)
            msg.setWindowTitle("Error de Seleccion")
            msg.setText("El directorio seleccionado no contiene imagenes para procesar")
            msg.setStandardButtons(QG.QMessageBox.Close)
            msg.exec_()

    def addListFiles(self, files):
        self.listFiles.clear()
        for filex in files:
            archivo = string.split(filex, "/")
            QG.QListWidgetItem(archivo[-1], self.listFiles)
        self.btn_viewimg.setEnabled(True)

    def changeConfProc(self):
        self.btn_p_selec.setEnabled(True)
        self.btn_p_cambiar.setEnabled(False)
        self.btn_archivo.setEnabled(False)

    def setConfProc(self):
        if self.nom_prueba.text() != "":
            self.btn_archivo.setEnabled(True)
            self.ext_file.setEnabled(True)
            self.btn_p_selec.setEnabled(False)
            self.btn_p_cambiar.setEnabled(True)
            self.npreguntas_spin.setEnabled(True)
        else:
            print "err"
            msg = QG.QMessageBox()
            msg.setIcon(QG.QMessageBox.Critical)
            msg.setWindowTitle("Error de Nombre")
            msg.setText("Debes Ingresar el nombre de la prueba")
            msg.setStandardButtons(QG.QMessageBox.Close)
            msg.exec_()

    def preProcess(self):
        self.DataPrueba = [self.nom_prueba.text()]

        if self.ArrayFiles != 0:
            self.ListArchivos.clear()
            self.process = Queue.Queue()
            self.DataQ = Queue.Queue()
            self.pbar_consola.setValue(0)
            for img in self.ArrayFiles:
                self.process.put(Thread_Proccessing(img, self.npreguntas_spin.value(), self.DataQ, self.pbar_consola, self.num_umbral.value()),
                                 False)
            self.pbar_consola.setMaximum(len(self.ArrayFiles))
            while self.process.empty() == False:
                thread = self.process.get(False)
                self.connect(thread, QC.SIGNAL("End"), self.writeDisplay)
                thread.start()
            self.export_test.setEnabled(True)


        else:
            print "err"
            msg = QG.QMessageBox()
            msg.setIcon(QG.QMessageBox.Critical)
            msg.setWindowTitle("Error")
            msg.setText("Se ha producido un error al cargar los archivos")
            msg.setStandardButtons(QG.QMessageBox.Close)
            msg.exec_()

    def writeDisplay(self):
        Data = self.DataQ.get(False)
        Alumno = QG.QTreeWidgetItem()
        yop = []
        yop.append(Data[0])
        if Data[0] == "00000000-0":
            Alumno.setText(0, "ERROR")
            Alumno.setText(1, u"Imagen Dañada")
            Alumno.setText(2, Data[2])
            self.ListArchivos.addTopLevelItem(Alumno)
        else:
            Alumno.setText(0, Data[0])
            Alumno.setText(1, "Alumno")
            Alumno.setText(2, Data[2])
            preg = 1
            for respuesta in Data[1]:
                itemresp = QG.QTreeWidgetItem(Alumno)
                itemresp.setText(0, str(preg))
                itemresp.setText(1, respuesta)
                preg += 1
                yop.append(respuesta)
            self.ListArchivos.addTopLevelItem(Alumno)


            self.DataPrueba.append([yop])

    def showImage(self):
        Item = self.listFiles.currentRow()
        imgView = Box_ImagenView(None, self.ArrayFiles[Item])
        imgView.exec_()

    def export_data(self):
        print self.DataPrueba
        try:
            name = str(self.txt_archivo.text())+ "/" +str(self.DataPrueba[0])+'.xlsx'
        except UnicodeEncodeError:
            name = str(self.txt_archivo.text())+ "/Datos_Exportados_ErrorEnNombre.xlsx"
        self.DataPrueba.pop(0)
        workbook = xlsxwriter.Workbook(name)
        worksheet = workbook.add_worksheet()
        row = 0
        for dat in self.DataPrueba:
            col = 0
            for txt in dat[0]:
                worksheet.write(row, col, txt)
                col += 1
            row += 1

        workbook.close()

        msg = QG.QMessageBox()
        msg.setIcon(QG.QMessageBox.Information)
        msg.setWindowTitle("Exportacion de archivo")
        msg.setText("La prueba fue exportada con exito.")
        msg.setStandardButtons(QG.QMessageBox.Close)
        msg.exec_()


class Thread_Proccessing(QC.QThread):
    def __init__(self, img, np, QueueData, bar, umbral):
        QC.QThread.__init__(self)
        self.img = img
        self.np = np
        self.Data = QueueData
        self.bar = bar
        self.umbral = umbral

    def __del__(self):
        self.wait()

    def run(self):
        value = self.bar.value() + 1
        self.bar.setValue(value)
        self.Data.put(eye.GetInfo(self.img, self.np, self.umbral), False)
        self.emit(QC.SIGNAL("End"))


class Box_ImagenView(QG.QDialog, Box_ImagenView_UI):
    def __init__(self, parent=None, img=None):
        QG.QDialog.__init__(self, parent)
        self.setupUi(self)
        self.img = img
        self.imagen = QG.QPixmap(img)

        self.close.clicked.connect(lambda: self.reject())

        self.img_sc = self.imagen.scaled(self.size(), QC.Qt.KeepAspectRatio)
        self.lbl_nombre.setText(self.img)
        self.img_view.setPixmap(self.img_sc)

    def resizeEvent(self, event=None):
        self.img_sc = self.imagen.scaled(self.size(), QC.Qt.KeepAspectRatio)
        self.img_view.setPixmap(self.img_sc)


class Registerkey(QG.QDialog, RegisterKey_UI):
    def __init__(self, parent=None, diskregister=None):
        self.dr = diskregister.replace("  \r\n","")
        QG.QDialog.__init__(self, parent)
        self.setupUi(self)
        self.cerrar.clicked.connect(lambda: self.reject())
        self.validate.clicked.connect(self.ValidateKey)
        self.continuar.clicked.connect(lambda: self.accept())

    def reject(self):
        sys.exit(301)

    def ValidateKey(self):

        try:
            cnx = _mysql.connect("190.114.255.73", "root", "", "FalconEye")
        except _mysql.OperationalError as err:
            self.status_key.setText("Error: No es posible conectarse a internet.")

        key = self.keyvalue.text()
        query = "SELECT * FROM datakey WHERE client_key='"+str(key)+"'"
        cnx.query(query)
        data = cnx.store_result()
        if data.num_rows()==1:
            datakey = data.fetch_row()[0]
            self.status_key.text="Registrando..."
            mx = datakey[2]
            query = "SELECT * FROM clients WHERE id_key="+datakey[0]
            cnx.query(query)
            keyused = cnx.store_result()
            nkused = keyused.num_rows()
            if nkused <= int(mx):
                self.status_key.setText("Activando "+str(keyused.num_rows()+1)+"/"+str(mx))
                query = "INSERT INTO clients (id_key, boardkey) VALUES(" + datakey[0]+",\'"+self.dr+"\')"
                cnx.query(query)
                secret = open("./check.xeye", "w")
                secret.write(key)
                secret.writelines("\n"+self.dr)
                self.status_key.setText("Activado - Presione continuar para utilizar el sistema")
                cnx.close()
                self.continuar.setEnabled(True)
            else:
                self.status_key.setText("Se ha alcanzado el maximo de registros para su numero de serie")
                cnx.close()
        else:
            self.status_key.setText("No existe el numero de serie")
            cnx.close()

myapp = QG.QApplication(sys.argv)
screen = Ventana(None)
sys.exit(myapp.exec_())
