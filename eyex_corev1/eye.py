import cv2
import numpy as np
import operator

def getPoints(zones, wi, hi, img_bw):
    pdix = calcPointsX(zones, wi, img_bw)
    pdiy = calcPointsY(zones, hi, img_bw)
    tango = True
    while tango:
        if len(pdix)==28:
            tango = False
        else:
            pdix = calcPointsX(zones, wi, img_bw)
    tango = True
    while tango:
        if len(pdiy) == 37:
            tango = False
        else:
            pdiy = calcPointsY(zones, hi, img_bw)

    return pdix, pdiy


def calcPointsX(zones, wi, img_bw):
    pdix = []
    zones[1] += 2
    p = 0
    for x in range(0,wi):
        if img_bw[zones[1],x]==0:
            #negro
            if (p+1)!=x:
                pdix.append(x)
                p = x
            else:
                p = x
    return pdix

def calcPointsY(zones, hi, img_bw):
    pdiy = []
    zones[0] += 2
    p = 0
    for y in range(0,hi):
        if img_bw[y,zones[0]]==0:
            #negro
            if (p+1)!=y:
                pdiy.append(y)
                p = y
            else:
                p = y
    return pdiy

def GetInfo(img, npreg, umbral):
    print "GetInfo: "+img
    img_rgb = cv2.imread(img)

    ##############################################################################

    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    img_gray = clahe.apply(img_gray)

    blur = cv2.GaussianBlur(img_gray,(5,5),0)
    ret3,img_bw = cv2.threshold(blur,0.92,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)


    wi, hi = img_gray.shape[::-1]
    cv2.rectangle(img_bw,(0,0),(wi,hi),255,45)

    pdix, pdiy= getPoints([0,0], wi, hi, img_bw)
    # print pdiy
    # print pdix
    #RUT##########################################################################
    RUT = ""
    campo = dict()
    imagenes = []
    for pos in range(10):
        if pos != 8:
            for pos2 in (range(11) if pos == 9 else range(10)):
                imagenes.append(img_bw[pdiy[pos2]:pdiy[pos2+1],pdix[pos]:pdix[pos+1]])
                xx, yy = imagenes[pos2].shape[::-1]
                cv2.rectangle(imagenes[pos2],(0,0),(xx,yy),255,8)

            digit = 1
            digit2 = 1
            for alt in imagenes:
                value = 0
                for dat in alt:
                    for dat2 in dat:
                        if dat2 == 0:
                             value+=1
                campo[(str(digit) if digit!=10 else "K")] = value
                                
                if digit == 9:
                    digit = 0
                else:
                    digit +=1

                if digit2 == 10:
                    digit= 10

                digit2 +=1

            print campo
            RUT = RUT + max(campo.iteritems(), key=operator.itemgetter(1))[0]
            del imagenes[:]
            campo.clear()
        else:
            RUT += "-"
    print RUT
    #############################################################################
    RESP = []
    # npreg = 37
    cols = [0,7,14,21]
    col = 0
    alternativas = []
    letra = ["A","B","C","D","E","x1","x2","x3"]
    campo = dict()
    y = 12
    for preg in xrange(1,npreg+1):
        alter = 0
        for x in xrange(cols[col],cols[col]+5):
            alternativas.append(img_bw[pdiy[y]:pdiy[y+1],pdix[x]:pdix[x+1]])
            #cv2.imshow(str(alter),img_bw[pdiy[y]:pdiy[(y+1)],pdix[x]:pdix[x+1]])
            xx, yy = alternativas[alter].shape[::-1]
            # cv2.rectangle(alternativas[alter],(0,0),(xx,yy),255,8)
            alter += 1
        l = 0
        if y<35:
            y += 1
        else:
            y = 11
        for alt in alternativas:
            digit = letra[l]
            value = 0
            for dat in alt:
                for dat2 in dat:
                    if dat2 == 0:
                         value+=1
            campo[str(digit)] = value
            l += 1
        dat = max(campo.iteritems(), key=operator.itemgetter(1))
        if dat[1]<=umbral :
            RESP.append("NA")
        else:
            RESP.append(dat[0])

        del alternativas[:]
        campo.clear()
        if preg%25 == 0:
            col +=1

    # for y in pdiy:
    #     cv2.circle(img_rgb, (pdix[0],y),1,(0,0,255),3)

    # cv2.imshow(str(img), img_rgb)

    return [RUT, RESP, img, dat]

# # print resp
# cv2.imwrite("imagentest.jpg", img_bw)
# key = cv2.waitKey(0)
# if key == ord('q'):
#         cv2.destroyAllWindows()
